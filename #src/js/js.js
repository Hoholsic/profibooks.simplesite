$( document ).ready(function() {
   $('.language_block').on('click', function(){ 
   		$('.list_language').slideToggle(500);
      $('.language_block').toggleClass('active');
   });
   $('.your-class').slick({
    infinite: true,
  	slidesToShow: 3,
  	slidesToScroll: 1,
    responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true
      }
    }
  ]

  });
   $('.slider-news').slick({
    infinite: true,
  	slidesToShow: 4,
  	slidesToScroll: 1,  
    prevArrow: $('#arrow-left'),
    nextArrow: $('#arrow-right'),
    responsive: [
    {
      breakpoint: 1000,
      settings: {
        variableWidth: true,
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 768,
      settings: {
        variableWidth: true,
        slidesToShow: 4,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 350,
      settings: {
        variableWidth: true,
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }
  ]
  });
   $('.slider-hits').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: $('#arrow-prew'),
    nextArrow: $('#arrow-next'),
    responsive: [
    {
      breakpoint: 1000,
      settings: {
        variableWidth: true,
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 768,
      settings: {
        variableWidth: true,
        slidesToShow: 4,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 350,
      settings: {
        variableWidth: true,
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }
  ]
  });
    $('.slider-elite').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: $('#button-prew'),
    nextArrow: $('#button-next'),
    responsive: [
    {
      breakpoint: 1000,
      settings: {
        variableWidth: true,
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 768,
      settings: {
        variableWidth: true,
        slidesToShow: 4,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 350,
      settings: {
        variableWidth: true,
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }
  ]
  });
  $('.btn-down').on('click', function(){ 
      $('.advanced-search').slideDown("slow");
      $('.advanced-search').removeClass('search_fixed search_mobil');
      $('.btn-down').addClass('btn-down_active');
      return false
  });
  $('.advanced-search_close-button').on('click', function(){ 
      $(' .advanced-search').slideUp("slow");
      $('.btn-down').removeClass('btn-down_active');
      return false
  });
   $('.btn-down_fixed').on('click', function(){
      $('.advanced-search_fixed').addClass('search_fixed');
      $('.advanced-search_close-button').addClass('close-fixed');
      $('.btn-down').addClass('btn-down_active');
       $('.overlay').show();
  });
  $('.close-fixed, .overlay').on('click', function(){
      $('.overlay').hide();
      $('.advanced-search_close-button').removeClass('close-fixed');
      $('.btn-down').removeClass('btn-down_active');
      $('.advanced-search_fixed').slideUp("slow");
  });
  $('.burger_book').on('click', function(){
     $('.burger-menu__list-book').slideToggle(800);
     $('.burger_book,.burger-menu__list-book').toggleClass('burger-active');
  });
    $('.header_burger-menu').on('click', function(){
        $('.header-burger_list').slideToggle(800);
        $('.header_burger-menu,.header-burger_list').toggleClass('burger-active');
        $('.overlay').toggle();
    });
    $('.overlay').on('click', function(){
        $('.header-burger_list').slideUp("slow");
        $('.header_burger-menu,.header-burger_list').removeClass('burger-active');
        $('.overlay').hide("slow");
    });
    $('#mobil_burger_book').on('click', function(){
        $('.burger-menu__list-book').slideDown("slow");
        $('#mobil_burger_book').addClass('btn-down_active');
        $('.overlay').show("slow");
    });
    $('.mobil_exit, .overlay').on('click', function(){
        $('.burger-menu__list-book').slideUp("slow");
        $('#mobil_burger_book').removeClass('btn-down_active');
    });
  $('#price_range').slider({
      range : true,
      min : 700,
      max : 2500,
      values : [700,2500],
      step : 1,
      orientation: "horizontal",
      animate: "slow",
      slide: function(event, ui) {
      $('#price_show').html(ui.values[0] + '-' + ui.values[1]);
      }
  });

  $('#pup-up_login, #pup-up_login2').on('click', function(){
     $('.overlay').show();
     $('#page-login').slideDown("slow");
     $('.header-burger_list').slideUp("slow");
     $('.header_burger-menu,.header-burger_list').removeClass('burger-active');
  });
  $('.overlay, .login_exit').on('click', function(){
      $('.overlay').hide(); 
      $('#page-login').slideUp("slow");
  });

  $('#registr').on('click', function(){ 
     $('.overlay').show();
     $('#page-login').slideUp("slow");
     $('#page-registr').slideDown("slow");
  });
  $('.overlay, .registr_exit').on('click', function(){ 
     $('.overlay').hide();
     $('#page-registr').slideUp("slow");
  });

  $('.basket-fixed, #pup-up_basket').on('click', function(){ 
     $('.overlay').show();
     $('.basket-page_fixed, #page-basket').slideDown("slow");
     return false
  });
  $('.overlay, .basket_exit').on('click', function(){ 
     $('.overlay').hide();
     $('#page-basket').slideUp("slow");
  });

  $('.basket_minus').click(function () {
        var $input = $(this).parent().find('.basket_value');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();

        var price = $(this).parents('.product-info').find('.book-price').data('default-price');
        console.log( price );
        basketPrice( price, count, this );


        return false;
    });
    $('.basket_plus').click(function () {
        var $input = $(this).parent().find('.basket_value');
        $input.val(parseInt($input.val()) + 1);
        $input.change();

        var price = $(this).parents('.product-info').find('.book-price').data('default-price');
        console.log( price );
        basketPrice( price, parseInt($input.val()), this )

        return false;
    });

    function basketPrice( price, productAmount, _this ){
      $(_this).parents('.product-info').find('.book-price span').text( price * productAmount );
      $(_this).parents('.basket-buy-book').find('.basket_product-coust span').text( price * productAmount );
    }

  $('.delete-product').click(function(){
    $('#product-group').hide();
  });

  $('#pup-up_feedback, #pup-up_feedback2').on('click', function(){
     $('.overlay').show();
     $('#page-feedback').slideDown("slow");
     $('.header-burger_list').slideUp("slow");
     $('.header_burger-menu,.header-burger_list').removeClass('burger-active');
  });
  $('.overlay, .feedback_exit').on('click', function(){ 
     $('.overlay').hide();
     $('#page-feedback').slideUp("slow");
  });

  $('#pup-up_feedback-up').on('click', function(){
     $('.overlay').show();
     $('#page-feedback-up').slideDown("slow");
  });
  $('.overlay, .feedback-up_exit').on('click', function(){ 
     $('.overlay').hide();
     $('#page-feedback-up').slideUp("slow");
  });
    $('.popup_pre-order').on('click', function(){
        $('.overlay').show();
        $('#page-pre-order').slideDown("slow");
        return false
    });
    $('.overlay, .pre-order_exit ').on('click', function(){
        $('.overlay').hide();
        $('#page-pre-order').slideUp("slow");
        return false
    });
  $('.pre-order_minus').click(function () {
        var $input = $(this).parent().find('.pre-order_value');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();

        var price = $(this).parents('.pre-order_product-info').find('.pre-order_book-price').data('default-price');
        console.log( price );
        setPrice( price, count, this );


        return false;
    });
    $('.pre-order_plus').click(function () {
        var $input = $(this).parent().find('.pre-order_value');
        $input.val(parseInt($input.val()) + 1);
        $input.change();

        var price = $(this).parents('.pre-order_product-info').find('.pre-order_book-price').data('default-price');
        console.log( price );
        setPrice( price, parseInt($input.val()), this )

        return false;
    });

    function setPrice( price, productAmount, _this ){
      $(_this).parents('.pre-order_product-info').find('.pre-order_book-price span').text( price * productAmount );
      $(_this).parents('.pre-order_buy-book').find('.pre-order_product-coust span').text( price * productAmount );
    }


  $('#pre-order_delete').click(function(){
    $('#pre-order_product').hide();
  });

  $('.feedback-btn').on('click', function(){
     $('.overlay').hide(); 
     $('#page-feedback').slideUp("slow");
     // $('.overlay').hide();
     $('#page-application').delay(3000).slideDown("slow");
     $('.overlay').delay(3000).show();
  });
  $('.application_exit, .overlay').on('click', function(){ 
     $('.overlay').hide();
     $('#page-application').slideUp("slow");
  });
  
  $(window).scroll(function(){
    if ($(this).scrollTop()<85) {
      $('.navigation-fixed').slideUp("fast");
    }
    else{$('.navigation-fixed').slideDown("fast")
    }
  });

    $('#mobil_search').on('click', function(){
        $('.mobil_search-list').slideToggle(800);
        $('.overlay').toggle();
    });
    $('.overlay').on('click', function(){
        $('.mobil_search-list').slideUp("slow");
        $('.overlay').hide("slow");
    });
    $('#mobil_btn-down').on('click', function(){
        $('.advanced-search').addClass('search_mobil');
        $('.advanced-search_close-button').addClass('close-mobil');
        $('.btn-down').addClass('btn-down_active');
    });
    $('.close-mobil, .overlay').on('click', function(){
        $('.advanced-search_close-button').removeClass('close-mobil');
        $('.btn-down').removeClass('btn-down_active');
        $('.advanced-search').slideUp("slow");
    });
});



